### CMake Configuration for the class DetectorConstructor

### Add the header for the DetectorConstructor class.
configure_file(DetectorConstructor.hpp
  ${CMAKE_BINARY_DIR}/include/DetectorConstructor.hpp
)

### Add DetectorConstructor class sources.
file(GLOB SOURCES DetectorConstructor.cpp)

### Add the DetectorConstructor class lib.
add_library(detector_constructor ${SOURCES})
