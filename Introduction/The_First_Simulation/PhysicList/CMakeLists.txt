### CMake Configuration for the class DetectorConstructor

### Add the header for the PhysicList class.
configure_file(PhysicList.hpp
  ${CMAKE_BINARY_DIR}/include/PhysicList.hpp
)

### Add PhysicList class sources.
file(GLOB SOURCES PhysicList.cpp)

### Add the PhysicList class lib.
add_library(physic_list ${SOURCES})
