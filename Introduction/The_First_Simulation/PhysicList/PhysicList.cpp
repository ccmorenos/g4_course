// Class header.
#include "PhysicList.hpp"

/**
* Constructor of the PhysicList class.
* Do nothing, just call the constructor of G4VUserPhysicsList class.
*/
PhysicList::PhysicList () : G4VUserPhysicsList() {}

/**
* Destructor of the PhysicList class.
* Do nothing.
*/
PhysicList::~PhysicList () {}

/**
* Add processes.
* It will the translation process.
*/
void *PhysicList::ConstructProcess () {
  AddTransportation();
}

//
// PhysicList.cpp end here
//
