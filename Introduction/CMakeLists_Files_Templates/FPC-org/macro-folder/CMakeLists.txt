### Configuration for the folders with macros.
file(GLOB MACROS *.mac)

foreach(MACRO ${MACROS})
  file(RELATIVE_PATH MACRO ${CMAKE_CURRENT_SOURCE_DIR} ${MACRO})
  configure_file(
    ${MACRO}
    ${CMAKE_BINARY_DIR}/G4Macros/${MACRO}
    COPYONLY
  )
endforeach()
